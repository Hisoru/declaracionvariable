
public class DeclaracionVariableApp {

	public static void main(String[] args) {
		
		// Declaración de una variable
		int N = 1;
		
		// Muestra por consola de operaciones
		System.out.println("Valor inicial de N = " + N);
		
		N += 77;
		System.out.println("N + 77 = " + N);
		
		N -= 3;
		System.out.println("N - 3 = " + N);
		
		N *= 2;
		System.out.println("N * 2 = " + N);

	}

}
